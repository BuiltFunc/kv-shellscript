#!/bin/sh

#----------------------------------------
# Initial setting
#----------------------------------------
cd $(dirname $0)

#----------------------------------------
# Variable definition
#----------------------------------------
declare readonly YUM_CMD=$(which yum)
declare readonly APT_GET_CMD=$(which apt-get)
declare readonly GLOBAL_IP=$(curl -s globalip.me)
declare readonly GROUP_NAME="group_sftp"
declare readonly NO_USER_NAMES=("root" "bin" "daemon" "adm" "lp" "sync" "shutdown" "halt" "mail" "operator" "games" "ftp" "nobody" "avahi-autoipd" "systemd-bus-proxy" "systemd-network" "dbus" "polkitd" "tss" "postfix" "sshd")
declare readonly CONFIG_FILE="addssh.config"
declare -i ERROR_FLG=false
declare USER_NAME

#----------------------------------------
# User check
#----------------------------------------
USER_NAME=$(whoami)
if [[ "$USER_NAME" = "root" ]]; then
  :
else
  echo -e "\nuser is "$USER_NAME"."
  echo -e "\nERROR: Please execute with root privileges."
  exit 255
fi

#----------------------------------------
# Read config file
#----------------------------------------
if [ ! -e "${CONFIG_FILE}" ]; then
  echo -e "\nERROR: The configuration file '${CONFIG_FILE}' does not exist."
  exit 255
fi
. "${CONFIG_FILE}"

#----------------------------------------
# Check the settings
#----------------------------------------
### User check
if [ -n "${USER}" ]; then
  if [[ "${USER}" =~ ^[a-z0-9_]{4,30}$ ]]; then
    if $(echo ${NO_USER_NAMES[@]} | grep -q "${USER}"); then
      ERROR_FLG=true
      echo -e "\nERROR: USER '${USER}' is a reserved word. \nPlease specify another user name."
    fi
  else
    ERROR_FLG=true
    echo -e "\nERROR: USER '${USER}' must be between 4 and 30 characters and a single-byte alphanumeric underscore."
  fi
else
  ERROR_FLG=true
  echo -e "\nERROR: USER does not exist."
fi

### Pass check
if [ -n "${PASS}" ]; then
  if [[ "${PASS}" =~ ^[a-z0-9_]{4,30}$ ]]; then
    :
  else
    ERROR_FLG=true
    echo -e "\nERROR: PASS '${PASS}' must be between 4 and 30 characters and a single-byte alphanumeric underscore."
  fi
else
  ERROR_FLG=true
  echo -e "\nERROR: PASS does not exist."
fi

### IP check
if [ ${#IPS[@]} ] >0; then
  for IP in ${IPS[@]}; do
    if [[ "${IP}" =~ ^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$ ]]; then
      :
    else
      ERROR_FLG=true
      echo -e "\nERROR: IP '${IP}' is not in the form of an IP address."
    fi
  done
else
  echo -e "\nERROR: IPS does not exist."
fi

### LETSENCRYPT_HOST check
if [ -z "${LETSENCRYPT_HOST}" ]; then
  ERROR_FLG=true
  echo -e "\nERROR: LETSENCRYPT_HOST does not exist."
fi

### LETSENCRYPT_EMAIL check
if [ -n "${LETSENCRYPT_EMAIL}" ]; then
  if [ $(echo ${LETSENCRYPT_EMAIL} | egrep -e '^[a-zA-Z0-9_+-]+(.[a-zA-Z0-9_+-]+)*@([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]*\.)+[a-zA-Z]{2,}$') ]; then
    :
  else
    ERROR_FLG=true
    echo -e "\nERROR: LETSENCRYPT_EMAIL '${LETSENCRYPT_EMAIL}' is not in the form of an email address."
  fi
else
  ERROR_FLG=true
  echo -e "\nERROR: LETSENCRYPT_EMAIL does not exist."
fi

### Exit if there is an error
if [ ${ERROR_FLG} = true ]; then
  echo -e ${ERROR_FLG}
  echo -e "\nERROR: The configuration file has been closed due to an error."
  exit 255
fi

#----------------------------------------
# User group existence confirmation
#----------------------------------------
if [ -z $(grep "^${GROUP_NAME}:" /etc/group) ]; then
  groupadd "${GROUP_NAME}"
fi

if [ -z $(grep "^${GROUP_NAME}:" /etc/passwd) ]; then
  useradd -g "${GROUP_NAME}" "${GROUP_NAME}"
fi

#----------------------------------------
# Option definition
#----------------------------------------
### Create help
function usage {
  cat <<EOM
Usage: $(basename "$0") [OPTION]...
  -h    help
  -r    Delete USER and execute
EOM

  exit 2
}

### Initialize
function reset {
  if [ $(grep "^${USER}:" /etc/passwd) ]; then
    docker stop $(docker ps -q)
    docker rm $(docker ps -q -a)
    userdel -r "${USER}"
    rm -rf "/etc/ssh/${USER}"
    rm -rf "/var/www"
    rm -rf "htpasswd"
    rm -rf "ssh_user"
  fi
}

### Processing definition by argument
while getopts ":rh" OPTION_KEY; do
  case "$OPTION_KEY" in
  r)
    reset
    ;;
  '-h' | '--help' | *)
    usage
    ;;
  esac
done

#----------------------------------------
# User existence confirmation
#----------------------------------------
if [ -z $(grep "^${USER}:" /etc/passwd) ]; then
  ### User created
  useradd "${USER}"

  if [ -z $(grep "^${USER}:" /etc/passwd) ]; then
    echo "\nERROR: Failed to create user '${USER}'."
    exit 255
  fi

  ### Public key creation
  if [ ! -e "/etc/ssh/${USER}" ]; then
    mkdir "/etc/ssh/${USER}"
    chown "${USER}:${USER}" "/etc/ssh/${USER}"
    chmod 701 "/etc/ssh/${USER}/.ssh"
  fi

  if [ ! -e "/etc/ssh/${USER}/.ssh" ]; then
    mkdir "/etc/ssh/${USER}/.ssh"
    chown "${USER}:${USER}" "/etc/ssh/${USER}/.ssh"
    chmod 700 "/etc/ssh/${USER}/.ssh"
  fi

  su - "${USER}" -c "ssh-keygen -f '/etc/ssh/${USER}/.ssh/id_rsa' -t rsa -N '' -m PEM;cat /etc/ssh/${USER}/.ssh/id_rsa.pub >> /etc/ssh/${USER}/.ssh/authorized_keys;chmod 600 /etc/ssh/${USER}/.ssh/authorized_keys"

  gpasswd -a "${USER}" "${GROUP_NAME}"

  ### id_rsa.pub copy
  if [ ! -e ssh_user ]; then
    mkdir ssh_user
  fi

  if [ ! -e "ssh_user/${USER}" ]; then
    mkdir "ssh_user/${USER}"
  fi

  if [ ! -e "ssh_user/${USER}/.ssh" ]; then
    mkdir "ssh_user/${USER}/.ssh"
  fi

  cp -af "/etc/ssh/${USER}/.ssh/id_rsa" "./ssh_user/${USER}/.ssh/id_rsa.pem"

fi

#----------------------------------------
# Create public directory
#----------------------------------------
if [ ! -e "/var/www" ]; then
  mkdir "/var/www"
  chmod 655 "/var/www"
  chown root:root "/var/www"
fi

if [ ! -e "/var/www/public" ]; then
  mkdir "/var/www/public"
  chmod 775 "/var/www/public"
  chown "${GROUP_NAME}:${GROUP_NAME}" "/var/www/public"
fi

#----------------------------------------
# Password setting
# Basic authentication
#----------------------------------------
echo "${USER}:${PASS}" | chpasswd

echo -e "USER=${USER}\nPASS=${PASS}\nSOURCE=/var/www/public\nLETSENCRYPT_HOST=${LETSENCRYPT_HOST}\nLETSENCRYPT_EMAIL=${LETSENCRYPT_EMAIL}\n" >|"./docker/.env"

if [ -e "htpasswd" ]; then
  rm -rf "htpasswd"
fi

mkdir "htpasswd"

if !(type "htpasswd" > /dev/null 2>&1); then
  if [[ ! -z $YUM_CMD ]]; then
    yum install -y httpd-tools
  elif [[ ! -z $APT_GET_CMD ]]; then
    apt-get install -y apache2-utils
  else
    echo -e "\nERROR: can't install command 'htpasswd'"
    exit 255
  fi
fi

htpasswd -b -c "htpasswd/${LETSENCRYPT_HOST}" "${USER}" "${PASS}"

# If you delete the container, you will get the LETSEN CRYPT again, so restart
if [ -z $(docker container ls -q -f name="nginx-proxy-lets-encrypt") ]; then
  docker-compose -f ./docker/docker-compose-ssl.yaml up -d
else
  docker-compose -f ./docker/docker-compose-ssl.yaml restart
fi

# If there is no container, start it, if there is, delete it and start it again
if [ -z $(docker container ls -q -f name="httpd") ]; then
  docker-compose -f ./docker/docker-compose.yaml --env-file ./docker/.env up -d
else
  docker-compose -f ./docker/docker-compose.yaml --env-file ./docker/.env stop httpd php
  docker-compose -f ./docker/docker-compose.yaml --env-file ./docker/.env rm -f httpd php
  docker-compose -f ./docker/docker-compose.yaml --env-file ./docker/.env up -d
fi

#----------------------------------------
# SSH IP settings
#----------------------------------------
### Limit the directories that users can access with sftp
if [ ! -e "/etc/ssh/sshd_config.bak" ]; then
  cp -f "/etc/ssh/sshd_config" "/etc/ssh/sshd_config.bak"
fi

cp -f "/etc/ssh/sshd_config.bak" "/etc/ssh/sshd_config"

echo -e "\n\nMatch Group ${GROUP_NAME}" >>/etc/ssh/sshd_config
echo "  ChrootDirectory /var/www" >>/etc/ssh/sshd_config
echo "  AuthorizedKeysFile /etc/ssh/%u/.ssh/authorized_keys" >>/etc/ssh/sshd_config
echo "  ForceCommand internal-sftp -u 002" >>/etc/ssh/sshd_config

systemctl restart sshd.service

#iptables -F
#for IP in ${IPS[@]}; do
#  iptables -A INPUT -p tcp --dport 22 -j ACCEPT -s "${IP}"
#done
#iptables -A INPUT -p tcp --dport 22 -j DROP
#iptables-save > /etc/sysconfig/iptables

### Output current settings:
#echo -e "\nSSH IP restriction execution result"
#iptables -nL | grep "dpt:22$"

echo -e "\n\n" >>/etc/ssh/sshd_config
echo "AllowUsers root" >>/etc/ssh/sshd_config
for IP in ${IPS[@]}; do
  echo "AllowUsers ${USER}@${IP}" >>/etc/ssh/sshd_config
done

systemctl restart sshd.service

echo -e "SUCCESS: Processing is complete"
exit 0
