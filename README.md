# kagoya-vps-docker-shellscript

カゴヤVPSサービス「Docker」で環境設定を行うスクリプト  
sftp用ユーザーを作成し、dokerを実行します。  
※ OSは、centos7のみ

![KAGOYA_CLOUD_VPS_コントロールパネル_2021-04-29](/uploads/8715bd4a83dfaccbd80b5265c9771697/KAGOYA_CLOUD_VPS_コントロールパネル_2021-04-29.jpg)

# 使い方

root権限でカゴヤVPSにログイン

```bash
# 初回gitが存在しないので、インストール
yum install -y git

# リポジトリをダウンロード
git clone https://gitlab.com/BuiltFunc/kv-shellscript.git
cd kv-shellscript

# 設定ファイルを作成
cp addssh-sample.config addssh.config
vi addssh.config

# スクリプトの実行
bash addssh.sh
```

# 注意点

## addssh.configについて
設定ファイル

* USER  
ユーザー名を記載  
4文字以上30文字以内、半角英数字アンダーバー  
ユーザー名はBasic認証とsshユーザーとして使用されます。  
```
USER="user"
```

* PASS  
Basic認証パスワード  
4文字以上30文字以内、半角英数字アンダーバー  
```
PASS="password"
```

* IPS  
許可IP一覧  
```
IPS=(
  "xxx.xxx.xxx.xxx"
  "xxx.xxx.xxx.xxx"
)
```

* LETSENCRYPT_HOST  
LETSENCRYPTでSSL接続するドメイン名  
```
LETSENCRYPT_HOST="example.com"
```

* LETSENCRYPT_EMAIL  
LETSENCRYPTで使用するメールアドレス  
```
LETSENCRYPT_EMAIL="info@example.com"
```

## スクリプトの実行について
sftpでの作業ディレクトリは、「/var/www/public」です。  
sftpでログインした場合「/var/www」の部分が削除され「/public」が作業用ディレクトリとなります。  
また、スクリプトを実行すると「./ssh_user/${USER}/.ssh/id_rsa.pem(以降、KEY)」が作成されます。  
KEYは、sftpでログインする鍵となります。

※${USER}は、addssh.configで設定したUSERの値がはいります。

# 課題

* 実行のログファイルの作成。
* Rootのアクセスをどうするか。
* 実行時、設定ファイルを保存してUSERの付き合わせをすることで必要なくなったユーザーを削除する。


# 作成者

* BuiltFunc - info@builtfunc.com

# License

"kagoya-vps-docker-shellscript" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
